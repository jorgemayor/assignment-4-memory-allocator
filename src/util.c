#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

extern inline size_t size_max( size_t x, size_t y );
