#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H

typedef bool (*test) (void);

extern test tests[];
extern int64_t tests_count;

#endif //MEMORY_ALLOCATOR_TESTS_H
